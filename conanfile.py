from conans import ConanFile, CMake
import os

class CrowConan(ConanFile):
    name = "crowex"
    version = "1.0"
    settings = "os", "compiler", "arch", "build_type"
    description = "CrowEx is C++ microframework for web. (inspired by Python Flask)"
    exports_sources = "include/*", "CMakeLists.txt"
    no_copy_sources = True
    short_paths = True
    license = "BSD-3-Clause"

    def build(self): # this is not building a library, just tests
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    # def package(self):
    #     self.copy("*.h", dst="include", src="include")
    def package(self):
        self.copy("*.h")

    def package_id(self):
        self.info.clear()